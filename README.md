This is the show directory for the TMC christmas light show.

To use it, download and install xLights from https://xlights.org/.

Clone this repository to a local dir on your system.
Start xlights and set the show directory to this directory.

Happy sequencing.

For (video) tutorials, manual and other information see https://xlights.org/